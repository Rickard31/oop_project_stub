//
// Created by vmarchenko on 11.12.18.
//

#include "../BigInteger.hpp"
#include <gtest/gtest.h>

using namespace stevepops;


TEST(ComparsionTest, EqualTest) {
    EXPECT_EQ(BigInteger(0), BigInteger(0));
    EXPECT_EQ(BigInteger(5ll), BigInteger(5));
    EXPECT_EQ(BigInteger(5ull), BigInteger(5ll));
    EXPECT_EQ(BigInteger(-5), BigInteger(-5ll));

    EXPECT_NE(BigInteger(1), BigInteger(0));
    EXPECT_NE(BigInteger(2), BigInteger(1));

    EXPECT_NE(BigInteger(6ll), BigInteger(5ll));
    EXPECT_NE(BigInteger(3ull), BigInteger(3 * UINT8_MAX));
    EXPECT_NE(BigInteger(-5ll), BigInteger(5));
}


TEST(ComparsionTest, LessThanTest) {
    EXPECT_LT(BigInteger(5), BigInteger(6));
    EXPECT_GE(BigInteger(5), BigInteger(5ull));
    EXPECT_LT(BigInteger(-5), BigInteger(0));
    EXPECT_LT(BigInteger(-5), BigInteger(-4));
    EXPECT_LT(BigInteger(-5), BigInteger(1));

    EXPECT_LT(BigInteger(255), BigInteger(256));
}


TEST(ComparsionTest, GreaterThanTest) {
    EXPECT_GT(BigInteger(6), BigInteger(5));
    EXPECT_GT(BigInteger(1), BigInteger(0));
    EXPECT_GT(BigInteger(1), BigInteger(-5));
    EXPECT_GT(BigInteger(0), BigInteger(-5));
    EXPECT_GT(BigInteger(-4), BigInteger(-5));
}


TEST(CalculationTest, AdditionTest) {
    BigInteger a = BigInteger(5) + BigInteger(6);
    EXPECT_EQ(a, BigInteger(11));
    EXPECT_GE(BigInteger(11), a);
    EXPECT_LE(a, BigInteger(11));

    BigInteger b = a += BigInteger(-22);
    EXPECT_EQ(b, BigInteger(-11));
    EXPECT_EQ(BigInteger(-11), b);

    b += BigInteger(-9);
    EXPECT_EQ(b, BigInteger(-20ll));

    b = b + BigInteger(30);
    EXPECT_EQ(b, BigInteger(10ll));

    BigInteger c = BigInteger(7) + BigInteger(0);
    EXPECT_EQ(BigInteger(7), c);

    BigInteger(d) = BigInteger(UINT8_MAX);
    ++d;
    EXPECT_EQ(BigInteger(1 << 8), d);
    EXPECT_EQ(++BigInteger(255), BigInteger(256));
    EXPECT_EQ(BigInteger(255) + BigInteger(256) + BigInteger(1), BigInteger(512));

    BigInteger e(-256);
    for (int i{-255}; i <= 256; ++i) {
        ++e;
        ASSERT_EQ(e, BigInteger(i));
    }
}


TEST(CalculationTest, SubtractionTest) {
    auto a = BigInteger(5) - BigInteger(0);
    EXPECT_EQ(a, BigInteger(5));

    a -= BigInteger(-5);
    EXPECT_EQ(a, BigInteger(10));

    a -= BigInteger(15);
    EXPECT_EQ(BigInteger(-5), a);

    auto b = a -= a;
    EXPECT_EQ(b, a);
    EXPECT_EQ(a, BigInteger(0));
    EXPECT_EQ(b, BigInteger(0));

    a += b;
    EXPECT_EQ(b, a);
    EXPECT_EQ(a, BigInteger(0));
    EXPECT_EQ(b, BigInteger(0));

    BigInteger e(256);
    for (int i{255}; i >= -256; i--) {
        --e;
        EXPECT_EQ(BigInteger(i), e);
    }
}


TEST(CalculationTest, MultiplicationTest) {
    BigInteger a(5);
    EXPECT_EQ(a * a, BigInteger(25));
    EXPECT_EQ(a * BigInteger(0), BigInteger(0));
    EXPECT_EQ(a * BigInteger(6), BigInteger(30));
    EXPECT_EQ(a * BigInteger(-6), BigInteger(-30));
    EXPECT_EQ(a * BigInteger(-5), BigInteger(-25));

    EXPECT_EQ(BigInteger(UINT8_MAX)*BigInteger(UINT8_MAX), BigInteger((int) UINT8_MAX * UINT8_MAX));

    BigInteger b(4ull);
    for (unsigned i{2}; i <= 15; ++i) {
        b *= BigInteger(4ll);
        EXPECT_EQ(BigInteger(1ull << (2 * i)), b);
    }
}


TEST (CalculationTest, DivisionTest) {
    BigInteger a(20);
    EXPECT_EQ (a/BigInteger(5), BigInteger(4));
    EXPECT_EQ((a/BigInteger(5))/BigInteger(4), BigInteger(1));
    EXPECT_EQ(a/BigInteger(21), BigInteger(0));
    EXPECT_EQ(a/BigInteger(10), BigInteger(2));
    EXPECT_EQ(a/BigInteger(-5), BigInteger(-4));
    EXPECT_EQ(BigInteger(100)/BigInteger(5), a);

    EXPECT_ANY_THROW(a/BigInteger(0));
}