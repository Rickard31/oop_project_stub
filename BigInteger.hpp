//
// Created by vmarchenko on 12/9/2018.
//

#ifndef BIGINTEGER_BIGINTEGER_HPP
#define BIGINTEGER_BIGINTEGER_HPP

#include <iostream>
#include <vector>
#include <cstdint>
#include <algorithm>

namespace stevepops {

    class BigInteger {
    public:

        //TODO constructors for other numeric types

        explicit BigInteger(int);

        explicit BigInteger(unsigned long long);

        explicit BigInteger(long long);

        BigInteger(const BigInteger &) = default;

        ~BigInteger() = default;

        BigInteger &operator++();

        BigInteger &operator--();

        BigInteger &operator=(const BigInteger &) = default;

        BigInteger &operator+=(const BigInteger &);

        BigInteger &operator*=(const BigInteger &);

        BigInteger &operator-=(const BigInteger &);

        BigInteger &operator/=(const BigInteger &);

        const bool isPositive() const { return sign_; }

        const std::size_t bytes_count() const { return bytes_.size(); }

        const bool bitAt(std::size_t index) const;

        const bool bitAt(std::size_t byte, std::size_t bit) const;

        const uint8_t byteAt(std::size_t index) const;

        /** Compare this number with another
         * @arg other - bytes of another BigInteger
         * @return -1 if other ones are greater, 0 if bytes are equal, 1 if these ones are greater
         */
        const int compare_to(const BigInteger &other) const;

        const std::vector<uint8_t> &bytes() const { return bytes_; }

    private:

        /** Compare bytes of this number with bytes of another
         * @arg other - bytes of another BigInteger
         * @return -1 if other ones are greater, 0 if bytes are equal, 1 if these ones are greater
         */
        const int byte_compare(const std::vector<uint8_t> &other_bytes) const;

        template<typename NumType>
        void fillBytes(NumType number);

        template<typename NumType>
        const int compare_to(NumType number) const;

        void add(const std::vector<uint8_t> &other_bytes);

        void subtract(const std::vector<uint8_t> &other_bytes);

        bool sign_;
        std::vector<uint8_t> bytes_;

    };

    std::ostream &operator<<(std::ostream &, const BigInteger &);

    const BigInteger operator++(BigInteger &, int);

    const BigInteger operator--(BigInteger &, int);

    const BigInteger operator+(const BigInteger &, const BigInteger &);

    const BigInteger operator-(const BigInteger &, const BigInteger &);

    const BigInteger operator*(const BigInteger &, const BigInteger &);

    const BigInteger operator/(const BigInteger &, const BigInteger &);

    const bool operator==(const BigInteger &, const BigInteger &);

    const bool operator!=(const BigInteger &, const BigInteger &);

    const bool operator<(const BigInteger &, const BigInteger &);

    const bool operator>(const BigInteger &, const BigInteger &);

    const bool operator<=(const BigInteger &, const BigInteger &);

    const bool operator>=(const BigInteger &, const BigInteger &);

//TODO operator overloads for BigInteger with other numeric types




    BigInteger::BigInteger(int num) : sign_(num >= 0), bytes_(sizeof(num), 0) {
        fillBytes(num < 0 ? -num : num);
    }

    BigInteger::BigInteger(long long num) : sign_(num >= 0), bytes_(sizeof(num), 0) {
        fillBytes(num < 0 ? -num : num);
    }

    BigInteger::BigInteger(unsigned long long num) : sign_(true), bytes_(sizeof(num), 0) {
        fillBytes(num);
    }

    BigInteger &BigInteger::operator++() {
        if (!isPositive()) {
            subtract({1});
        } else {
            add({1});
        }
        return *this;
    }

    BigInteger &BigInteger::operator--() {
        if (isPositive()) {
            subtract({1});
        } else {
            add({1});
        }
        return *this;
    }

    BigInteger &BigInteger::operator+=(const BigInteger &b) {
        if (isPositive() == b.isPositive()) {
            this->add(b.bytes_);
        } else {
            this->subtract(b.bytes_);
        }
        return *this;
    }

    BigInteger &BigInteger::operator*=(const BigInteger &b) {
        //TODO adequate implementation
        BigInteger zero(0);
        if (b == zero) {
            sign_ = true;
            bytes_ = {0};
            return *this;
        }

        BigInteger temp(b);

        if (!b.isPositive()) {
            temp.sign_ = true;
            sign_ = !sign_;
        }

        BigInteger old(*this);

        for (BigInteger i(1); i < temp; ++i) {
            *this += old;
        }

        return *this;
    }

    BigInteger &BigInteger::operator-=(const BigInteger &b) {
        if (this == &b) {
            return (*this) = BigInteger(0);
        }

        if (isPositive() && b.isPositive()) {
            this->subtract(b.bytes_);
        } else if (isPositive() && !b.isPositive()) {
            this->add(b.bytes_);
        } else if (!isPositive() && b.isPositive()) {
            this->add(b.bytes_);
        } else {
            this->subtract(b.bytes_);
        }
        return *this;
    }

    BigInteger &BigInteger::operator/=(const BigInteger &other) {
        if (other==BigInteger(0))
            throw "Tried to divide by zero";

        if (byte_compare(other.bytes()) < 0) {
            sign_ = true;
            bytes_ = {0};
            return *this;
        }

        bool new_sign = (isPositive()==other.isPositive());
        sign_ = other.sign_;
        BigInteger res(0);
        while(byte_compare(other.bytes())>=0) {
            *this -= other;
            ++res;
        }
        sign_ = new_sign;
        bytes_ = res.bytes();
        return *this;
    }

    const uint8_t BigInteger::byteAt(std::size_t index) const {
        if (index >= bytes_.size())
            return 0;
        return bytes_[index];
    }

    const bool BigInteger::bitAt(std::size_t index) const {
        std::size_t byte{index / 8};
        std::size_t bit{index % 8};
        return bitAt(byte, bit);
    }

    const bool BigInteger::bitAt(std::size_t byte, std::size_t bit) const {
        int b{byteAt(byte)};
        int t{1 << bit};
        t &= b;
        return (t > 0);
    }

    const int BigInteger::compare_to(const BigInteger &other) const {

        if (isPositive() && !other.isPositive()) {
            return 1;
        } else if (!isPositive() && other.isPositive()) {
            return -1;
        }

        int byte_cmp = byte_compare(other.bytes_);

        if (isPositive() && other.isPositive()) {
            return byte_cmp;
        }
        return -byte_cmp;
    }

    template<typename NumType>
    void BigInteger::fillBytes(NumType number) {
        bytes_.clear();
        if (number == 0) {
            bytes_.push_back(0);
            return;
        }
        while (number > 0) {
            bytes_.push_back(number % (UINT8_MAX+1));
            number /= (UINT8_MAX+1);
        }
    }

    template <typename NumType>
    const int BigInteger::compare_to(NumType number) const {
        if (this->isPositive() && number < 0)
            return 1;
        if (!(this->isPositive()) && number>=0)
            return -1;

        std::vector<uint8_t> num_bytes;
        if (number==0) {
            num_bytes.push_back(0);
        } else {
            while (number > 0) {
                num_bytes.push_back(number % (UINT8_MAX+1));
                number /= (UINT8_MAX+1);
            }
        }
        int res = byte_compare(num_bytes);
        if (!isPositive())
            res = -res;
        return res;
    }

    const int BigInteger::byte_compare(const std::vector<uint8_t> &other_bytes) const {
        if (bytes_.size() > other_bytes.size()) {
            for (size_t i = bytes_.size() - 1; i >= other_bytes.size(); --i) {
                if (bytes_[i] > 0)
                    return 1;
            }
        } else if (other_bytes.size() > bytes_.size()) {
            for (size_t i = other_bytes.size(); i >= bytes_.size(); --i) {
                if (other_bytes[i] > 0)
                    return -1;
            }
        }
        for (size_t i = std::min(bytes_.size(), other_bytes.size()) - 1; i > 0; --i) {
            if (bytes_[i] > other_bytes[i])
                return 1;
            if (other_bytes[i] > bytes_[i])
                return -1;
        }
        if (bytes_[0] > other_bytes[0])
            return 1;
        if (other_bytes[0] > bytes_[0])
            return -1;
        return 0;
    }

    void BigInteger::add(const std::vector<uint8_t> &other_bytes) {
        while (bytes_.size() < other_bytes.size()) {
            bytes_.push_back(0);
        }
        uint8_t overflow{0};
        for (size_t i = 0; i < other_bytes.size(); ++i) {
            if (overflow > 0) {
                uint8_t temp = bytes_[i] + overflow;
                overflow = 0;
                if (temp < bytes_[i]) {
                    ++overflow;
                }
                bytes_[i] = temp;
            }
            uint8_t temp = bytes_[i] + other_bytes[i];
            if (temp < bytes_[i] or temp < other_bytes[i]) {
                ++overflow;
            }
            bytes_[i] = temp;
        }

        for (size_t i{other_bytes.size()}; i < bytes_.size() && overflow > 0; ++i) {
            uint8_t temp = bytes_[i] + overflow;
            overflow = 0;
            if (temp < bytes_[i]) {
                ++overflow;
            }
            bytes_[i] = temp;
        }

        if (overflow > 0) {
            bytes_.push_back(overflow);
        }
    }

    void BigInteger::subtract(const std::vector<uint8_t> &other_bytes) {
        int cmp = byte_compare(other_bytes);
        if (cmp == 0) {
            sign_ = true;
            bytes_ = {0};
        } else if (cmp > 0) {
            uint8_t overflow{0};
            for (size_t i{0}; i < other_bytes.size(); ++i) {
                if (overflow > 0) {
                    uint8_t temp = bytes_[i] - overflow;
                    overflow = 0;
                    if (temp > bytes_[i]) {
                        ++overflow;
                    }
                    bytes_[i] = temp;
                }
                uint8_t temp = bytes_[i] - other_bytes[i];
                if (temp > bytes_[i]) {
                    ++overflow;
                }
                bytes_[i] = temp;
            }

            for (size_t i{other_bytes.size()}; i < bytes_.size() && overflow > 0; ++i) {
                uint8_t temp = bytes_[i] - overflow;
                overflow = 0;
                if (temp > bytes_[i]) {
                    ++overflow;
                }
                bytes_[i] = temp;
            }
            while (bytes_.back() == 0)
                bytes_.pop_back();
        } else if (cmp < 0) {
            std::vector<uint8_t> this_bytes(bytes_);
            bytes_ = other_bytes;
            sign_ = !sign_;
            subtract(this_bytes);
        }
    }

    std::ostream &operator<<(std::ostream &os, const BigInteger &bi) {
        os << (bi.isPositive() ? '+' : '-');
        os << "( ";
        for (uint8_t i:bi.bytes()) {
            os << static_cast<unsigned>(i) << ' ';
        }
        os << ")";
        return os;
    }

    const BigInteger operator++(BigInteger &bi, int) { return ++bi; }

    const BigInteger operator--(BigInteger &bi, int) { return --bi; }

    const BigInteger operator+(const BigInteger &a, const BigInteger &b) { return BigInteger(a) += b; }

    const BigInteger operator-(const BigInteger &a, const BigInteger &b) { return BigInteger(a) -= b; }

    const BigInteger operator*(const BigInteger &a, const BigInteger &b) { return BigInteger(a) *= b; }

    const BigInteger operator/(const BigInteger &a, const BigInteger &b) {return BigInteger(a) /= b; }

    const bool operator==(const BigInteger &a, const BigInteger &b) { return a.compare_to(b) == 0; }

    const bool operator!=(const BigInteger &a, const BigInteger &b) { return !(a == b); }

    const bool operator<(const BigInteger &a, const BigInteger &b) { return a.compare_to(b) < 0; }

    const bool operator>(const BigInteger &a, const BigInteger &b) { return a.compare_to(b) > 0; }

    const bool operator<=(const BigInteger &a, const BigInteger &b) { return !(a > b); }

    const bool operator>=(const BigInteger &a, const BigInteger &b) { return !(a < b); }


}

#endif //BIGINTEGER_BIGINTEGER_HPP