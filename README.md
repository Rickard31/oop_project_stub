<b>This is a stub for multi-platform CMake project which uses GTest.</b>


<b>Platform Details</b>

This should work on any platform without limitations.
If you have GTest installed, CMake should simply link it.
If you don't, CMake will compile GTest from sources.


<b>Usage Details</b>

After building the project you should have two run configurations - <i>oop_project_stub</i> (for running main.cpp) and <i>run_all_tests</i> (for running all tests located in tests/)


<b>Adding your own code </b>

CMake will automatically add all contents of tests/ as gtest files and all headers and sources of core directory as project files.
So you can add new sources, headers and files without need to get into cmake.




<a href="https://t.me/steve_pops">https://t.me/steve_pops</a>

<a href="mailto:vitalka.marchenko@gmail.com">vitalka.marchenko@gmail.com</a>